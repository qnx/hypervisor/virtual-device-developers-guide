<pre><code lang="C">

# QNX Hypervisor Virtual Device Development

These files contain source code examples for customized QNX Hypervisor virtual device development.
Make sure to download the source code archive that matches your version of QNX Hypervisor

QNX_Hypervisor_8.0_Virtual_Device_Developers_Guide_src_examples_V1.0   [zip]
    This zip file contains source examples for QNX Hypervisor 8.0
        vdev-trace          (for guest debugging)
        vdev-ser8250        (serial port example showing connection to back end driver)
        vdev-virtio-console (very useful to show how virtqueues work) 
        vdev-virtio-entropy (random number support)
        vdev-ib700          (watchdog emulating a hware device)
        vdev-sp805          (watchdog emulating a hware device)

        Download the zip file, unpack to your working area. Be sure to set your compile path to QNX SDP8.
        Then you can 'make clean' and 'make' in the root of each example source folder



QNX_Hypervisor_2.2_Virtual_Device_Developers_Guide_src_examples_V1.0   [tar]
    This tar file contains source examples for QNX Hypervisor 2.X as well as QNX Hypervisor for Safety 2.X
        Look at the README.txt file in the tarball for build instructions
        vdev-trace          (for guest debugging)
        vdev-ser8250        (serial port example showing connection to back end driver)
        vdev-virtio-console (very useful to show how virtqueues work) 
        vdev-virtio-entropy (random number support)
        vdev-ib700          (watchdog emulating a hware device)
        vdev-sp805          (watchdog emulating a hware device)


</code></pre>
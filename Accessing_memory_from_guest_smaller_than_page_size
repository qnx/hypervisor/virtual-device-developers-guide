'''

// This code will not compile as is (it uses a private header), but it is provided to show how the guest can access regions
// of memory smaller than 4K
// If you need this functionality, talk to us. Sometimes in complex sharing models you need pass-thru of memory less than 4K
// so that multiple guests can interact (via a vdev) to the same 4k page

/*
 * $QNXLicenseC:
 * Copyright 2018, QNX Software Systems. All Rights Reserved.
 *
 * You must obtain a written license from and pay applicable license fees to QNX
 * Software Systems before you may reproduce, modify or distribute this software,
 * or any work that includes all or part of this software.   Free development
 * licenses are available for evaluation and non-commercial purposes.  For more
 * information visit http://licensing.qnx.com or email licensing@qnx.com.
 *
 * This file may contain contributions from others.  Please review this entire
 * file for other proprietary rights or license notices, as well as the QNX
 * Development Suite License Guide at http://licensing.qnx.com/license-guide/
 * for other information.
 * $
 */

/**
 * @file
 * @brief   fine-grained (but slow) memory pass-through
 */

#include <sys/mman.h>
#include <string.h>
#include <qvm/vdev-core.h>
#include <qvm/guest.h>
#include <qvm/log.h>
#include <qvm/utils.h>
#include <private/vm_tree.h>

enum {
    OPT_LOC_IDX,
    OPT_PERM_IDX,
    OPT_NUM_OPTS
};

#define PERM_IGNORE 0x0001

struct mempass_perm {
    vm_tree_node_t  node;
    unsigned        start;
    unsigned        end;
    int             prot;
};

struct mempass_state {
    uint8_t             *host_region;
    vm_tree_t           perms;
    struct mempass_perm def_perm;
};

enum allow_type {
    ALLOW_IGNORE,
    ALLOW_REJECT,
    ALLOW_PASS
};

/**
 * Implement vm_tree_func_t::cmp_node.
 * @param   __n     node to insert
 * @param   __p     node to compare to
 * @retval  0 if they overlap
 * @retval -1 if search should go on to the left
 * @retval 1 if search should go on to the right
 */
static int
perm_cmp(void * const __n, void * const __p)
{
    const struct mempass_perm * const n = __n;
    const struct mempass_perm * const p = __p;
    if (n->end < p->start) return -1;
    if (n->start > p->end) return 1;
    return 0;
}

/**
 * Implement vm_tree_func_t::cmp_value.
 * @param   __n     node to check
 * @param   __v     value to check
 * @retval  0 if the value belongs to the node
 * @retval -1 if search should go on to the left
 * @retval 1 if search should go on to the right
 */
static int
perm_value(void * const __n, uintptr_t const __v)
{
    const struct mempass_perm * const n = __n;
    unsigned const offset = (unsigned)__v;
    if (offset < n->start) return -1;
    if (offset > n->end) return 1;
    return 0;
}

/**
 * Implement vm_tree_func_t::cmp_range.
 * @param   __n     node to check
 * @param   __s     start of range to check
 * @param   __e     end of range to check
 * @retval  0 if the node overlaps with the range
 * @retval -1 if search should go on to the left
 * @retval 1 if search should go on to the right
 */
static int
perm_range(void * const __n, uintptr_t const __s, uintptr_t const __e)
{
    const struct mempass_perm * const n = __n;
    unsigned const start = (unsigned)__s;
    unsigned const end = (unsigned)__e;
    if (end < n->start) return -1;
    if (start > n->end) return 1;
    return 0;
}

/**
 * Check if a guest transaction is allowed per the user's configuration.
 * @param   vdp     virtual device
 * @param   vopnd   block pointed to by the transaction
 * @param   prot    request type
 * @param   poffset offset into the device given @p vopnd
 * @return  an appropriate ::allow_type value.
 */
static enum allow_type
allowed(const vdev_t * const vdp, const struct qvm_state_block * const vopnd, int const prot,
    unsigned * const poffset)
{
    uint64_t const loffset = vopnd->location - vdp->v_block.location;
    if ((loffset + vopnd->length) > vdp->v_block.length) return ALLOW_REJECT;

    unsigned offset = (unsigned)loffset;
    unsigned const end = offset + (unsigned)vopnd->length;
    *poffset = offset;

    const struct mempass_state * const state = vdp->v_device;
    enum allow_type result = ALLOW_PASS;
    while (offset < end) {
        const struct mempass_perm *perm = vm_tree_find_value(&state->perms, offset);
        if (perm == NULL) {
            perm = &state->def_perm;
        }
        if ((perm->prot & prot) == 0) {
            if (perm->prot & PERM_IGNORE) {
                result = ALLOW_IGNORE;
            } else {
                return ALLOW_REJECT;
            }
        }
        offset = perm->end + 1;
    }

    return result;
}

/**
 * Perform a vdev read operation for the device
 * @param   vdev    virtual device
 * @param   cookie  region cookie
 * @param   vopnd   state block for virtual device reference operand
 * @param   oopnd   state block for the 'other' operand
 * @param   gcp     guest cpu performing the reference
 * @returns VRS_* value
 */
static vdev_ref_status_t
vdmempass_vread(vdev_t *const vdev, unsigned const cookie,
             const struct qvm_state_block *const vopnd,
             const struct qvm_state_block *const oopnd,
             struct guest_cpu *const gcp)
{
    unsigned offset;
    switch (allowed(vdev, vopnd, PROT_READ, &offset))
    {
    case ALLOW_IGNORE:
        guest_write_all_ones(gcp, GXF_NONE, oopnd);
        return VRS_NORMAL;

    case ALLOW_REJECT:
        return VRS_BUS_ERROR;

    default:
        break;
    }

    const struct mempass_state * const state = vdev->v_device;

    pthread_mutex_lock(&vdev->v_mtx);
    guest_cpu_write(gcp, GXF_NONE, oopnd, 1, state->host_region + offset, vopnd->length);
    pthread_mutex_unlock(&vdev->v_mtx);

    return VRS_NORMAL;
}

/**
 * Perform a vdev write operation for the device
 * @param   vdev    virtual device
 * @param   cookie  region cookie
 * @param   vopnd   state block for virtual device reference operand
 * @param   oopnd   state block for the 'other' operand
 * @param   gcp     guest cpu performing the reference
 * @returns VRS_* value
 */
static vdev_ref_status_t
vdmempass_vwrite(vdev_t *const vdev, unsigned const cookie,
             const struct qvm_state_block *const vopnd,
             const struct qvm_state_block *const oopnd,
             struct guest_cpu *const gcp)
{
    unsigned offset;
    switch (allowed(vdev, vopnd, PROT_WRITE, &offset))
    {
    case ALLOW_IGNORE:
        return VRS_NORMAL;

    case ALLOW_REJECT:
        return VRS_BUS_ERROR;

    default:
        break;
    }

    const struct mempass_state * const state = vdev->v_device;

    pthread_mutex_lock(&vdev->v_mtx);
    guest_cpu_read(gcp, GXF_NONE, oopnd, 1, state->host_region + offset, vopnd->length);
    pthread_mutex_unlock(&vdev->v_mtx);

    return VRS_NORMAL;
}

/**
 * Add a new node into the permission tree.
 * @param   state   virtual device state
 * @param   start   first offset covered
 * @param   end     last offset covered
 * @param   prot    allowed accesses (combination, possibly empty, of PROT_READ and PROT_WRITE)
 */
static void
add_perm(struct mempass_state * const state, unsigned const start, unsigned const end,
    int const prot)
{
    struct mempass_perm * const node = malloc(sizeof(*node));
    if (node == NULL) {
        qvm_fatal(QLS_QVM, "not enough memory for mempass node allocation");
    }
    node->start = start;
    node->end = end;
    node->prot = prot;
    vm_tree_insert(&state->perms, &node->node);
}

/**
 * Handle one "perm" option.
 * @param   state   virtual device state
 * @param   start   first offset covered
 * @param   end     last offset covered
 * @param   prot    allowed accesses (combination, possibly empty, of PROT_READ and PROT_WRITE)
 */
static void
handle_perm(struct mempass_state * const state, unsigned start, unsigned const end,
    int const prot)
{
    struct mempass_perm *prev_node = NULL;
    if (start > 0) {
        prev_node = vm_tree_find_value(&state->perms, start-1);
        if ((prev_node != NULL) && (prev_node->prot != prot)) {
            unsigned const prev_node_end = prev_node->end;
            prev_node->end = start-1;
            if (prev_node_end > end) {
                add_perm(state, end+1, prev_node_end, prev_node->prot);
            }
            prev_node = NULL;
        }
    }

    while (start <= end) {
        // prev_node is a node that ends just before start, with a compatible prot
        // start is the beginning of the range we want to cover, end is the last covered offset
        struct mempass_perm * const node = vm_tree_find_range(&state->perms, start, end);
        if (node == NULL) {
            if (prev_node != NULL) {
                prev_node->end = end;
            } else {
                add_perm(state, start, end, prot);
            }
            break;
        }

        unsigned const node_end = node->end;
        if (node->prot == prot) {
            if (prev_node != NULL) {
                vm_tree_remove(&state->perms, &node->node);
                free(node);
                prev_node->end = node_end;
            } else {
                node->start = start;
                prev_node = node;
            }
            start = node_end + 1;
            continue;
        }

        if (end < node_end) {
            node->start = end + 1;
            if (prev_node != NULL) {
                prev_node->end = end;
            } else {
                add_perm(state, start, end, prot);
            }
            break;
        }

        if (prev_node != NULL) {
            vm_tree_remove(&state->perms, &node->node);
            free(node);
            prev_node->end = node_end;
        } else {
            node->start = start;
            node->prot = prot;
        }

        start = node_end + 1;
    }
}

/**
 * Parse the flags of a `perm' option.
 * @param   perm    potential flags
 * @return  the translated set of PROT_READ and PROT_WRITE flags.
 */
static int
parse_perm_flags(const char *perm)
{
    if (*perm == ',') {
        int prot = 0;
        ++perm;
        while (*perm != '\0') {
            switch (*perm) {
            case 'r':
                prot |= PROT_READ;
                break;
            case 'w':
                prot |= PROT_WRITE;
                break;
            case 'i':
                prot |= PERM_IGNORE;
                break;
            default:
                qvm_fatal(QLS_CNF, "unknown permission flag `%c'", *perm);
            }
            ++perm;
        }
        return prot;
    }

    if (*perm != '\0') {
        qvm_fatal(QLS_CNF, "Illegal trailing character");
    }

    return 0;
}

/**
 * Parse the flags of a `loc' option.
 * The result is stored in mempass_state::def_perm.
 * @param   state   state of the vdev
 * @param   str     contents of the sub-section of the option argument
 * @return  a pointer to the next part in str.
 */
static const char *
parse_loc_flags(struct mempass_state * const state, const char *str)
{
    state->def_perm.prot = 0;
    unsigned adding = 1;
    for (;;) {
        char const ch = *str;
        int flag = 0;
        if (ch == 'r') {
            flag = PROT_READ;
        } else if (ch == 'w') {
            flag = PROT_WRITE;
        } else if (ch == 'c') {
            flag = PROT_NOCACHE;
        } else if (ch == 'i') {
            flag = PERM_IGNORE;
        } else if (ch == '-') {
            adding = 0;
        } else if (ch == '+') {
            adding = 1;
        } else {
            break;
        }

        if (adding) {
            state->def_perm.prot |= flag;
        } else {
            state->def_perm.prot &= ~flag;
        }

        ++str;
    }
    // PROT_NOCACHE meaning is inverted in the loop for simplicity
    state->def_perm.prot ^= PROT_NOCACHE;
    return str;
}

/**
 * Handle process-container operations.
 * @param   vdp     virtual device
 * @param   ctrl    operation
 * @param   arg     parameters for the operation
 * @return  EOK on success, an error otherwise.
 */
static int
vdmempass_control(vdev_t * const vdp, unsigned const ctrl, const char * const arg) {
    struct mempass_state *state = vdp->v_device;
    const char *str;
    uint64_t length;

    switch (ctrl) {
    case VDEV_CTRL_OPTIONS_START:
        vdp->v_block.length = 0;
        vdp->v_block.qst_type = QST_MEMORY;

        static vm_tree_func_t perm_funcs = {
            .cmp_nodes = perm_cmp,
            .cmp_value = perm_value,
            .cmp_range = perm_range
        };
        vm_tree_init(&state->perms, &perm_funcs, offsetof(struct mempass_perm, node));
        break;

    case VDEV_CTRL_FIRST_OPTIDX + OPT_LOC_IDX:
        if (state->host_region != NULL) {
            munmap(state->host_region, vdp->v_block.length);
        }

        str = qvm_parse_block(arg, &vdp->v_block);
        if (vdp->v_block.qst_type != QST_MEMORY) {
            qvm_fatal(QLS_CNF, "cannot pass-through `%s'",
                qvm_block_type_name(vdp->v_block.qst_type));
        }
        if (*str != ',') {
            qvm_fatal(QLS_CNF, "`loc' option needs a length");
        }
        ++str;
        length = (uint64_t)UINT32_MAX;
        str = qvm_parse_num(str, QPNF_ALLOW_SUFFIXES | QPNF_SIZE_CHECK | QPNF_ALLOW_TRAILING,
            &length);
        if (length == 0) {
            qvm_fatal(QLS_CNF, "length must be grater than zero");
        }
        vdp->v_block.length = (unsigned)length;
        state->def_perm.start = 0;
        state->def_perm.end = (unsigned)length-1;

        if (*str == ',') {
            str = parse_loc_flags(state, ++str);
        } else {
            state->def_perm.prot = PROT_NOCACHE | PROT_READ | PROT_WRITE;
        }

        uint64_t host_paddr = vdp->v_block.location;
        if (*str == '=') {
            ++str;
            qvm_parse_num(str, QPNF_ALLOW_SUFFIXES, &host_paddr);
        } else {
            if (*str != '\0') {
                qvm_fatal(QLS_CNF, "illegal characters trailing `host' option");
            }
        }

        state->host_region = mmap(NULL, length, PROT_READ | PROT_WRITE |
            (state->def_perm.prot & PROT_NOCACHE), MAP_SHARED | MAP_PHYS, NOFD, (off_t)host_paddr);
        if (state->host_region == MAP_FAILED) {
            qvm_fatal(QLS_CNF, "can't map %lx: %s", host_paddr, strerror(errno));
        }
        break;

    case VDEV_CTRL_FIRST_OPTIDX + OPT_PERM_IDX: ;
        uint64_t offset = vdp->v_block.length;
        str = qvm_parse_num(arg, QPNF_ALLOW_SUFFIXES | QPNF_SIZE_CHECK | QPNF_ALLOW_TRAILING,
            &offset);
        if (*str != ',') {
            qvm_fatal(QLS_CNF, "`perm' option needs to include a length");
        }

        ++str;
        length = (uint64_t)(vdp->v_block.length - offset) + 1;
        const char * const perm = qvm_parse_num(str,
            QPNF_ALLOW_SUFFIXES | QPNF_SIZE_CHECK | QPNF_ALLOW_TRAILING, &length);
        int const prot = parse_perm_flags(perm);
        handle_perm(state, (unsigned)offset, (unsigned)(offset + length - 1), prot);
        break;

    default:
        break;
    }

    return EOK;
}


/**
 * Register the virtual device into the framework.
 */
static void __attribute__((constructor))
vdmempass_register(void) {
    static const char *const mempass_options[OPT_NUM_OPTS+1] = {
        [OPT_LOC_IDX]  =    "loc",
        [OPT_PERM_IDX] =    "perm",
        [OPT_NUM_OPTS] =    NULL,
    };
    static struct vdev_factory vdmempass_factory = {
        .next = NULL, // patched
        .control = vdmempass_control,
        .vread = vdmempass_vread,
        .vwrite = vdmempass_vwrite,
        .option_list = mempass_options,
        .name = NULL, // patched
        .factory_flags = VFF_INTR_NO | VFF_MUTEX | VFF_VDEV_MEM,
        .acc_sizes = (1u << 1) | (1u << 2) | (1u << 4) | (1u << 8),
        .extra_space = sizeof(struct mempass_state),
    };
    vdev_register_factory(&vdmempass_factory, QVM_VDEV_ABI);
}

'''